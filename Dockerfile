FROM alpine

ENV TEST_STRING="update 5"

ENTRYPOINT ["/bin/sh", "-c"]
CMD ["echo ${TEST_STRING}"]
