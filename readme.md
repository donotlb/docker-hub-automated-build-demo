# docker-hub-automated-build-demo

**放弃 docker hub / docker cloud 的 自动构建了!!!! 太慢、bug 太多!!!!**

## docker hub 自动构建配置

![](assets/readme-4080259e.png)

**注意: Tag 正则表达式为 `/^[0-9\.]+$/` 不要漏掉后面的 +**

## 触发自动构建并检验结果

Dockerfile 的内容如下:  

```
FROM alpine

ENV TEST_STRING="update 1"

ENTRYPOINT ["/bin/sh", "-c"]
CMD ["echo ${TEST_STRING}"]
```

演示步骤:  

1.  改变环境变量 `TEST_STRING` 的值
2.  git commit + git tag + git push
3.  检验 docker hub 中自动构建的状态

每一次 git push 后, 都会触发 docker hub 的自动构建任务, 自动构建任务会产出两个镜像, 这两个镜像实际是同一个, 但版本号不同, 一个是 latest, 另一个与 git master 分支的 tag 相同.

![](assets/readme-5d829dc2.png)
